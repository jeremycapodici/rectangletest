
function redSquare(){
	var example = document.getElementById('maincanvas');
	var context = example.getContext('2d');
	context.fillStyle = "rgb(255,0,0)";
	context.fillRect(30, 30, 50, 50);
}
function yellowSquare(){
	var example = document.getElementById('maincanvas');
	var context = example.getContext('2d');
	context.fillStyle = "rgb(255,255,0)";
	context.fillRect(50, 50, 70, 70);
}
function makeSquare(){
	var example = document.getElementById('maincanvas');
	var context = example.getContext('2d');
	var startX = document.getElementById('startX');
	var startY = document.getElementById('startY');
	var widthX = document.getElementById('widthX');
	var heightY = document.getElementById('heightY');
	var rotate = document.getElementById('heightY');

	var redColor = document.getElementById('redColor');
	var greenColor = document.getElementById('greenColor');
	var blueColor = document.getElementById('blueColor');
	context.fillStyle = "rgb(" + redColor.value + ","+ greenColor.value + ","+ blueColor.value+")";
	context.fillRect(startX.value, startY.value,widthX.value, heightY.value);
}
//http://www.html5canvastutorials.com/advanced/html5-clear-canvas/
function clearCanvas(){
    var canvas = document.getElementById("maincanvas");
    var context = canvas.getContext("2d");
    context.clearRect(0, 0, canvas.width, canvas.height);
}
//http://danielmclaren.com/node/90
function saveCanvasToDivOutput(){
    var canvas = document.getElementById("maincanvas");
    var base64 = canvas.toDataURL();
    var container = document.getElementById('output');
    container.innerHTML = base64;
    container.innerHTML = "<img class=outputdiv src='"+base64+"'/>"
	+"<br>base64:<input  type=textarea value='" + base64+"'/>";
}